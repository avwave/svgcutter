import svgwrite
import base64
from cStringIO import StringIO
from PIL import Image
import argparse

class SVGCutter(object):
    svg_document = None

    def __init__(self, src_img, dst_img):
        img = Image.open(src_img);
        size = img.size

        output = StringIO()
        img.save(output, format='JPEG')
        im_data = output.getvalue()

        data_url = 'data:image/png;base64,' + base64.b64encode(im_data)

        self.svg_document = svgwrite.Drawing(filename=dst_img,
                                             size=(size[0], size[1]))

        self.svg_document.add(self.svg_document.image(href=data_url,
                                                      insert=(0, 0),
                                                      size=size))

        self.svg_document.add(self.svg_document.circle(center=(size[0]/2, size[1]/2),
                                                       r=size[0]/2,
                                                       fill_opacity=0,
                                                       stroke='rgb(230, 0, 126)',
                                                       stroke_width=0.25,

                                                       id='CutContour'))

        # print (self.svg_document.tostring())

        self.svg_document.save()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="source image")
    parser.add_argument("-o", "--output", help="output svg name")
    args = parser.parse_args()

    if not args.output:
        print "Using default output.svg"
        args.output = 'output.svg'


    svg_cutter = SVGCutter(args.input, args.output)

if __name__ == '__main__':
    main()
